# -*- coding: utf-8 -*-
import secrets
import argparse
from pathlib import Path

from ruamel.yaml import YAML

def get_dicts_list():
    """Parse .yaml files and place it in list, where every element is a dict
    parsed from .yaml file.
    Pretty ugly and slow.
    """
    # loading of a document without resolving unknow tags
    yaml = YAML(typ='safe')

    # Path.glob() is a generator. Return list of Path-like objects
    # which indicate our dict_**.yaml files
    dicts_path_list = Path('dicts/').glob('*.yaml')

    dicts_list = []
    for words_dict in dicts_path_list:
        dicts_list.append(yaml.load(words_dict))
    return dicts_list


def drop_dice():
    """For each of the 5 dices generate a secure random number
    (secrets.randbelow starts from 0, so we need to add + 1)
    and place it in list.
    """
    dices = [str(secrets.randbelow(5) + 1) for dice in range(5)]
    return dices


def main():
    parser = argparse.ArgumentParser(description='Generate secure CHBS password')
    parser.add_argument('--num',
                        type=int, default=4, help='number of words')
    parser.add_argument('--sep',
                        default='-', help='separator')
    args = parser.parse_args()

    dicts_list = get_dicts_list()
    passphrase = []

    for _ in range(args.num):
        random_dict = secrets.choice(dicts_list)
        dices = drop_dice()
        random_word = random_dict[''.join(dices)]
        passphrase.append(str(random_word))
    print(args.sep.join(passphrase))

if __name__ == '__main__':
    main()
